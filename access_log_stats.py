#!/usr/bin/env python
"""
Tooling used to parse access log entries.
Use `./access_log_stats.py --help` to get usage string.
"""
from collections import Counter
import argparse
import datetime as dt
import re
import requests
import sys

DT_FMT = '%d/%b/%Y:%H:%M:%S %z'
BAD_AGENTS = ['bot', 'spider', 'crawler']
TOP_N = 10
IP_API = 'https://ipapi.co/{ip}/json/'
ACCESS_LOG_RE = r'^[^:]+:(?P<ip>\d+\.\d+\.\d+\.\d+) - - \[(?P<datetime>[^\]]+)\] "(?P<method>[^ ]+) (?P<url>[^ ]+) [^ ]+ (?P<status>\d{3}) [^ ]+ [^ ]+ (?P<agent>.+)$'


class Entry:
    def __init__(self, raw_entry='', regex='', dt_fmt=''):
        self.raw_entry = raw_entry
        self.regex = regex
        self.dt_fmt = dt_fmt
        self.ip_address = ''
        self.datetime = None
        self.raw_datetime = ''
        self.http_method = ''
        self.requested_url = ''
        self.http_status_code = ''
        self.user_agent = ''

        if self.regex:
            self._parse_entry()
        else:
            try:
                ip, raw_dt, method, url, status, agent = self.raw_entry.split('|')
            except ValueError:
                print(f'error with: {self.raw_entry}')
            else:
                self.ip_address = ip
                self.raw_datetime = raw_dt
                self.datetime = self._parse_datetime(self.raw_datetime)
                self.http_method = method
                self.requested_url = url
                self.http_status_code = status
                self.user_agent = agent

    @classmethod
    def parse_file(cls, filename, regex, dt_fmt):
        with open(filename) as access_logs:
            entries = [cls(line, regex, dt_fmt) for line in access_logs]
            return entries

    @classmethod
    def read_file(cls, filename, dt_fmt):
        with open(filename) as access_logs:
            entries = [cls(line, dt_fmt=dt_fmt) for line in access_logs]
            return entries

    def __str__(self):
        return f'{self.ip_address}|{self.raw_datetime}|{self.http_method}|{self.requested_url}|{self.http_status_code}|{self.user_agent}'

    def _parse_datetime(self, raw_datetime):
        return dt.datetime.strptime(raw_datetime, self.dt_fmt)

    def _parse_entry(self):
        self.raw_entry = self.raw_entry.strip()
        try:
            fields = re.match(self.regex, self.raw_entry).groupdict()
            self.ip_address = fields.get('ip', '')
            self.raw_datetime = fields.get('datetime', '')
            self.datetime = self._parse_datetime(self.raw_datetime)
            self.http_method = fields.get('method', '')
            self.requested_url = fields.get('url', '')
            self.http_status_code = fields.get('status', '')
            self.user_agent = fields.get('agent', '').strip().strip('"').strip()
        except IndexError:
            print(f'ERROR: could not parse: {self.raw_entry}')


class AccessSummary:
    def __init__(self, entries, top_n, check_ip_location, bad_urls, bad_agents):
        self.entries = entries
        self.top_n = top_n
        self.check_ip_location = check_ip_location
        self.bad_urls = bad_urls
        self.bad_agents = bad_agents

    def generate(self):
        valid_accesses = 0
        earliest_access = self.entries[0].datetime
        urls = []
        non_200_responses = []
        ips = []

        for entry in self.entries:
            if any(bad in entry.user_agent.lower() for bad in self.bad_agents):
                continue
            if any(entry.requested_url.startswith(bad) for bad in self.bad_urls):
                continue
            if entry.http_status_code not in ['200', '301', '302']:
                non_200_responses.append(entry)
                continue

            valid_accesses += 1
            urls.append(entry.requested_url)
            ips.append(entry.ip_address)
            if entry.datetime < earliest_access:
                earliest_access = entry.datetime

        urls = Counter(urls)
        ip_addresses = Counter(ips).most_common(self.top_n)
        top_ip_requests = sum(c for _, c in ip_addresses)
        percent_from_top = (top_ip_requests/valid_accesses) * 100
        if self.check_ip_location:
            ip_info = self.request_ip_info(ip_addresses)

        earliest = dt.datetime.strftime(earliest_access, entry.dt_fmt)
        num_non_200 = len(non_200_responses)
        last_n = self.top_n if num_non_200 > self.top_n else num_non_200
        summary = [
            f'Summary stats from {earliest} to today:',
            '=======================================',
            f' Number of valid requests: {valid_accesses}',
            f' Top {self.top_n} urls:',
        ] + [
            f'  - {url} - {count}'
            for url, count in urls.most_common(self.top_n)
        ] + [
            f' Num non 200/301/302 responses: {num_non_200}',
            f' Last {last_n} non 200/301/302 responses:',
        ] + [
            f'  - {a.requested_url} ({a.ip_address}) -> {a.http_status_code} ({a.raw_datetime})'
            for a in non_200_responses[-1 * last_n:]
        ] + [
            f' Top {self.top_n} IP addresses make up {percent_from_top:.1f}% of the requests.',
        ]
        if self.check_ip_location:
            for ip, count, country in ip_info:
                summary.append(f'  - {ip} - {country} [{count}]')

        return '\n'.join(summary)

    def __str__(self):
        return self.generate()

    def request_ip_info(self, ip_addresses):
        info = []
        for ip, count in ip_addresses:
            country = self.get_country(ip)
            info.append((ip, count, country))
        return info

    @staticmethod
    def get_country(ip):
        response = requests.get(IP_API.format(ip=ip))
        if response.ok:
            info = response.json()
            return f'{info.get("country_name", "unknown")} ({info.get("org", "unknown")})'
        else:
            return 'unknown'


def argparser(raw_args):
    parser = argparse.ArgumentParser(
        description='Parse access logs and get stats.'
    )
    parser.add_argument(
        'access_log_filename', help='The access log file to parse.'
    )
    parser.add_argument(
        '-p',
        '--preparsed',
        action='store_true',
        default=False,
        help='Has the access log file already been parsed? Default False',
    )
    parser.add_argument(
        '--bad-url',
        action='append',
        default=[],
        help='Ignore URLs if they start with any of these strings. Default []',
    )
    parser.add_argument(
        '--clear-bad-agents',
        action='store_true',
        default=False,
        help='Start bad-agents as a empty list. Default False',
    )
    parser.add_argument(
        '--bad-agent',
        action='append',
        default=BAD_AGENTS,
        help=f'Ignore requests if the agent string contains these words. Default {BAD_AGENTS}',
    )
    parser.add_argument(
        '-r',
        '--regex',
        default=ACCESS_LOG_RE,
        help=fr'The regex string to be used when parsing logs. Should contain named groups for: ip, datetime, method, url, status, and agent. Default {ACCESS_LOG_RE}',
    )
    parser.add_argument(
        '-d',
        '--datetime-fmt',
        default=DT_FMT,
        help=fr'The datetime format to use when parsing dates from the logs. Default {DT_FMT.replace("%", "%%")}',
    )
    parser.add_argument(
        '-f',
        '--filter',
        action='store_true',
        default=False,
        help='Filter entries based on bad-urls and bad-agents. Default False',
    )
    parser.add_argument(
        '-s',
        '--summary',
        action='store_true',
        default=False,
        help='Print summary statistics. Default False',
    )
    parser.add_argument(
        '-n',
        '--top-n',
        type=int,
        default=10,
        help='The number of top urls, ips, agents to display in the summary. Default 10',
    )
    parser.add_argument(
        '-c',
        '--check-ip-location',
        action='store_true',
        default=False,
        help='Should the IP location be queried from the IP API service. Default False',
    )

    return parser.parse_args(raw_args)


def main(raw_args):
    args = argparser(raw_args)
    access_log_filename = args.access_log_filename
    preparsed = args.preparsed
    bad_urls = args.bad_url
    bad_agents = args.bad_agent
    if args.clear_bad_agents:
        bad_agents = [x for x in bad_agents if x not in BAD_AGENTS]
    regex = args.regex
    datetime_fmt = args.datetime_fmt
    summary = args.summary
    filter = args.filter
    top_n = args.top_n
    check_ip_location = args.check_ip_location

    if not preparsed:
        entries = Entry.parse_file(access_log_filename, regex, datetime_fmt)
    else:
        entries = Entry.read_file(access_log_filename, datetime_fmt)

    if summary:
        print(AccessSummary(entries, top_n, check_ip_location, bad_urls, bad_agents))
    else:
        for entry in entries:
            if filter and any(bad in entry.user_agent.lower() for bad in bad_agents):
                continue
            if filter and any(entry.requested_url.startswith(bad) for bad in bad_urls):
                continue
            print(entry)


if __name__ == '__main__':
    main(sys.argv[1:])
